<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab*/
$lang ['tab_news'] = "News";
$lang ['tab_forum'] = "Forum";
$lang ['tab_store'] = "Store";
$lang ['tab_bugtracker'] = "Bugtracker";
$lang ['tab_changelogs'] = "Changelogs";
$lang ['tab_pvp_statistics'] = "Statistiques JcJ";
$lang ['tab_login'] = "Connexion";
$lang ['tab_register'] = "Registrar";
$lang ['tab_home'] = "Démarrer";
$lang ['tab_donate'] = "Faire un don";
$lang ['tab_vote'] = "Votez";
$lang ['tab_cart'] = "Panier";
$lang ['tab_account'] = "Mon compte";
$lang ['tab_reset'] = "Récupération de mot de passe";
$lang ['tab_error'] = "Erreur 404";
$lang ['tab_maintenance'] = "Maintenance";
$lang ['tab_online'] = "Joueurs en ligne";

/* Panneau Navbar */
$lang ['navbar_vote_panel'] = "Panneau de vote";
$lang ['navbar_donate_panel'] = "Panneau de don";

/* Bouton Lang */
$lang ['button_register'] = "Registrar";
$lang ['button_login'] = "Connexion";
$lang ['button_logout'] = "Fermer la session";
$lang ['button_forgot_password'] = "Vous avez oublié votre mot de passe?";
$lang ['button_user_panel'] = "Panneau utilisateur";
$lang ['button_admin_panel'] = "Panneau d'administration";
$lang ['button_mod_panel'] = "Panneau de modération";
$lang ['button_change_avatar'] = "Changer l'avatar";
$lang ['button_add_personal_info'] = "Ajouter des informations personnelles";
$lang ['button_create_report'] = "Créer un rapport";
$lang ['button_new_topic'] = "Nouveau thème";
$lang ['button_edit_topic'] = "Modifier le thème";
$lang ['button_save_changes'] = "Enregistrer les modifications";
$lang ['button_cancel'] = "Annuler";
$lang ['button_send'] = "Soumettre";
$lang ['button_read_more'] = "En savoir plus";
$lang ['button_add_reply'] = "Créer une réponse";
$lang ['button_remove'] = "Supprimer";
$lang ['button_create'] = "Créer";
$lang ['button_save'] = "Enregistrer";
$lang ['button_close'] = "Fermer";
$lang ['button_reply'] = "Répondre";
$lang ['button_donate'] = "Faire un don";
$lang ['button_account_settings'] = "Paramètres du compte";
$lang ['button_cart'] = "ajouter au panier";
$lang ['button_view_cart'] = "Voir le panier";
$lang ['button_checkout'] = "Commander";
$lang ['button_buying'] = "Continuer les achats";

/* Lang d'alerte */
$lang ['alert_successful_purchase'] = "Article acheté avec succès.";
$lang ['alert_upload_error'] = "Votre image doit être au format .jpg";
$lang ['alert_changelog_not_found'] = "Le serveur n'a aucun journal des modifications à signaler pour le moment";
$lang ['alert_points_insufficient'] = "Runes insuffisantes";

/* Statut Lang */
$lang ['offline'] = "Hors ligne";
$lang ['online'] = "En ligne";

/* Étiquette Lang */
$lang ['label_open'] = "Ouvrir";
$lang ['label_closed'] = "Fermé";

/* Étiquette de formulaire Lang */
$lang ['label_login_info'] = "Informations sur le journal";

/* Entrée espace réservé Lang */
$lang ['placeholder_username'] = "Nom d'utilisateur";
$lang ['placeholder_email'] = "Email";
$lang ['placeholder_password'] = "Mot de passe";
$lang ['placeholder_re_password'] = "Répéter le mot de passe";
$lang ['placeholder_current_password'] = "Mot de passe actuel";
$lang ['placeholder_new_password'] = "Nouveau mot de passe";
$lang ['placeholder_new_username'] = "Nouvel utilisateur";
$lang ['placeholder_confirm_username'] = "Confirmer le nouvel utilisateur";
$lang ['placeholder_new_email'] = "Nouvel e-mail";
$lang ['placeholder_confirm_email'] = "Confirmer le nouvel e-mail";
$lang ['placeholder_create_bug_report'] = "Créer un rapport d'erreur";
$lang ['placeholder_title'] = "Titre";
$lang ['placeholder_type'] = "Type";
$lang ['placeholder_description'] = "Description";
$lang ['placeholder_url'] = "URL";
$lang ['placeholder_uri'] = "URL conviviale (exemple: toux)";
$lang ['placeholder_highl'] = "En vedette";
$lang ['placeholder_lock'] = "Lock";
$lang ['placeholder_subject'] = "Objet";

/* En-tête de tableau Lang */
$lang ['table_header_name'] = "Nom";
$lang ['table_header_faction'] = "Faction";
$lang ['table_header_total_kills'] = "Nombre total de décès";
$lang ['table_header_today_kills'] = "Décès aujourd'hui";
$lang ['table_header_yersterday_kills'] = "Décès hier";
$lang ['table_header_team_name'] = "Nom de l'équipe";
$lang ['table_header_members'] = "Membres";
$lang ['table_header_rating'] = "Index";
$lang ['table_header_games'] = "Jeux";
$lang ['table_header_id'] = "ID";
$lang ['table_header_status'] = "Statut";
$lang ['table_header_priority'] = "Priorité";
$lang ['table_header_date'] = "Date";
$lang ['table_header_author'] = "Auteur";
$lang ['table_header_time'] = "Heure";
$lang ['table_header_icon'] = "Icône";
$lang ['table_header_realm'] = "Royaume";
$lang ['table_header_zone'] = "Zone";
$lang ['table_header_character'] = "Caractère";
$lang ['table_header_price'] = "Prix";
$lang ['table_header_item_name'] = "Nom de l'article";
$lang ['table_header_items'] = "Article (s)";
$lang ['table_header_quantity'] = "Quantité";

/*Class Lang*/
$lang ['class_warrior'] = "Warrior";
$lang ['class_paladin'] = "Paladin";
$lang ['class_hunter'] = "Hunter";
$lang ['class_rogue'] = "Voleur";
$lang ['class_priest'] = "Priest";
$lang ['class_dk'] = "Chevalier de la mort";
$lang ['class_shamman'] = "Chaman";
$lang ['class_mage'] = "Magicien";
$lang ['class_warlock'] = "Warlock";
$lang ['class_monk'] = "Moine";
$lang ['class_druid'] = "Druide";
$lang ['class_demonhunter'] = "Chasseur de démons";

/* Faction Lang */
$lang ['faction_alliance'] = "Alliance";
$lang ['faction_horde'] = "Horde";

/* Sexe Lang */
$lang ['gender_male'] = "Homme";
$lang ['gender_female'] = "Femme";

/* Race Lang */
$lang ['race_human'] = "Humain";
$lang ['race_orc'] = "Orc";
$lang ['race_dwarf'] = "Nain";
$lang ['race_night_elf'] = "Elfe de la nuit";
$lang ['race_undead'] = "Undead";
$lang ['race_tauren'] = "Tauren";
$lang ['race_gnome'] = "Gnome";
$lang ['race_troll'] = "Trol";
$lang ['race_goblin'] = "Goblin";
$lang ['race_blood_elf'] = "Elfe de sang";
$lang ['race_draenei'] = "Draenei";
$lang ['race_worgen'] = "Worgen";
$lang ['race_panda_neutral'] = "Pandaren Neutral";
$lang ['race_panda_alli'] = "Pandaren Alliance";
$lang ['race_panda_horde'] = "Pandaren Horde";
$lang ['race_nightborde'] = "Nightborne";
$lang ['race_void_elf'] = "Elfe du Vide";
$lang ['race_lightforged_draenei'] = "Lightforged Draenei";
$lang ['race_highmountain_tauren'] = "Tauren monte alto";
$lang ['race_dark_iron_dwarf'] = "Dark Iron Dwarf";
$lang ['race_maghar_orc'] = "Orc Maghar";

/* En-tête Lang */
$lang ['header_cookie_message'] = "Ce site utilise des cookies pour vous garantir la meilleure expérience sur notre site.";
$lang ['header_cookie_button'] = "J'ai compris!";

/* Pied de page Lang */
$lang ['footer_rights'] = "Tous droits réservés.";

/* Page 404 Lang */
$lang ['page_404_title'] = "404 Page non trouvée";
$lang ['page_404_description'] = "Il semble que la page que vous recherchez est introuvable";

/* Langage du panneau utilisateur */
$lang ['panel_acc_rank'] = "Plage de comptes";
$lang ['panel_dp'] = "Runes";
$lang ['panel_vp'] = "Points de vote";
$lang ['panel_expansion'] = "Expansion";
$lang ['panel_member'] = "Membre depuis";
$lang ['panel_chars_list'] = "Liste de caractères";
$lang ['panel_account_details'] = "Détails du compte";
$lang ['panel_last_ip'] = "Dernière IP";
$lang ['panel_change_email'] = "Changer l'email";
$lang ['panel_change_password'] = "Changer le mot de passe";
$lang ['panel_replace_pass_by'] = "Remplacer le mot de passe par";
$lang ['panel_current_username'] = "Nom d'utilisateur actuel";
$lang ['panel_current_email'] = "Adresse e-mail actuelle";
$lang ['panel_replace_email_by'] = "Remplacer l'e-mail par";

/* Accueil Lang */
$lang ['home_latest_news'] = "Dernières nouvelles";
$lang ['home_discord'] = "Discord";
$lang ['home_server_status'] = "Statut du serveur";
$lang ['home_realm_info'] = "Actuellement le royaume est";

/* Lang PvP Statistics */
$lang ['statistics_top_20'] = "TOP 20";
$lang ['statistics_top_2v2'] = "TOP 2V2";
$lang ['statistics_top_3v3'] = "TOP 3V3";
$lang ['statistics_top_5v5'] = "TOP 5V5";

/* Nouvelles Lang */
$lang ['news_recent_list'] = "Liste des nouvelles récentes";
$lang ['news_comments'] = "Commentaires";

/* Bugtracker Lang */
$lang ['bugtracker_report_notfound'] = "Rapports non trouvés";

/* Faire un don Lang */
$lang ['donate_get'] = "Get";

/* Votez Lang */
$lang ['vote_next_time'] = "Prochain vote sur:";

/* Forum Lang */
$lang ['forum_post_count'] = "messages";
$lang ['forum_topic_locked'] = "Ce sujet est fermé.";
$lang ['forum_comment_locked'] = "Quelque chose à expliquer? Entrez pour participer. ";
$lang ['forum_comment_header'] = "Rejoignez la conversation";
$lang ['forum_not_authorized'] = "Non autorisé";
$lang ['forum_post_history'] = "Afficher l'historique du sujet";
$lang ['forum_topic_list'] = "Liste des sujets";
$lang ['forum_last_activity'] = "Dernière activité";
$lang ['forum_last_post_by'] = "Dernier message de";
$lang ['forum_whos_online'] = "Qui est en ligne";
$lang ['forum_replies_count'] = "Réponses";
$lang ['forum_topics_count'] = "Sujets";
$lang ['forum_users_count'] = "Utilisateurs";

/* Store Lang */
$lang ['store_categories'] = "Catégories de magasins";
$lang ['store_top_items'] = "TOP Articles";
$lang ['store_cart_added'] = "Vous avez ajouté";
$lang ['store_cart_in_your'] = "dans votre panier";
$lang ['store_cart_no_items'] = "Vous n'avez aucun article dans votre panier.";

/* Savon Lang */
$lang ['soap_send_subject'] = "Stocker les articles";
$lang ['soap_send_body'] = "Merci de magasiner dans notre magasin et de soutenir le serveur pour rester en ligne!";

/* Courriel Lang */
$lang ['email_password_recovery'] = "Récupération de mot de passe";
$lang ['email_account_activation'] = "Activation du compte";
