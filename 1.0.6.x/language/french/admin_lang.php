<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang ['admin_nav_dashboard'] = "Panneau";
$lang ['admin_nav_system'] = "Système";
$lang ['admin_nav_manage_settings'] = "Gérer les paramètres";
$lang ['admin_nav_manage_modules'] = "Gérer les modules";
$lang ['admin_nav_users'] = "Utilisateurs";
$lang ['admin_nav_accounts'] = "Comptes";
$lang ['admin_nav_website'] = "Site Web";
$lang ['admin_nav_menu'] = "Menu";
$lang ['admin_nav_realms'] = "Royaumes";
$lang ['admin_nav_slides'] = "Slides";
$lang ['admin_nav_news'] = "Actualités";
$lang ['admin_nav_changelogs'] = "Changelogs";
$lang ['admin_nav_pages'] = "Pages";
$lang ['admin_nav_donate_methods'] = "Méthodes de don";
$lang ['admin_nav_topsites'] = "Topsites";
$lang ['admin_nav_donate_vote_logs'] = "Journaux de dons / votes";
$lang ['admin_nav_store'] = "Store";
$lang ['admin_nav_manage_store'] = "Gérer le magasin";
$lang ['admin_nav_forum'] = "Forum";
$lang ['admin_nav_manage_forum'] = "Gérer le forum";
$lang ['admin_nav_logs'] = "Log System";

/*Sections Lang*/
$lang ['section_general_settings'] = "Paramètres généraux";
$lang ['section_module_settings'] = "Paramètres du module";
$lang ['section_optional_settings'] = "Paramètres optionnels";
$lang ['section_seo_settings'] = "Paramètres SEO";
$lang ['section_update_cms'] = "Mettre à jour le CMS";
$lang ['section_check_information'] = "Vérifier les informations";
$lang ['section_forum_categories'] = "Catégories du forum";
$lang ['section_forum_elements'] = "Éléments du forum";
$lang ['section_store_categories'] = "Catégories de magasins";
$lang ['section_store_items'] = "Articles de magasin";
$lang ['section_store_top'] = "TOP articles du magasin";
$lang ['section_logs_dp'] = "Journaux de dons";
$lang ['section_logs_vp'] = "Journaux de vote";

/*Button Lang*/
$lang ['button_select'] = "Sélectionner";
$lang ['button_update'] = "Mettre à jour";
$lang ['button_unban'] = "Déséquilibre";
$lang ['button_ban'] = "Ban";
$lang ['button_remove'] = "Supprimer";
$lang ['button_grant'] = "Grant";
$lang ['button_update_version'] = "Mettre à jour vers la dernière version";

/* En-tête de tableau Lang */
$lang ['table_header_subcategory'] = "Sélectionnez une sous-catégorie";
$lang ['table_header_race'] = "Race";
$lang ['table_header_class'] = "Classe";
$lang ['table_header_level'] = "Niveau";
$lang ['table_header_money'] = "Argent";
$lang ['table_header_time_played'] = "Temps joué";
$lang ['table_header_actions'] = "Actions";
$lang ['table_header_id'] = "#ID";
$lang ['table_header_tax'] = "Taxes";
$lang ['table_header_points'] = "Points";
$lang ['table_header_type'] = "Type";
$lang ['table_header_module'] = "Module";
$lang ['table_header_payment_id'] = "ID de paiement";
$lang ['table_header_hash'] = "Hachage";
$lang ['table_header_total'] = "Total";
$lang ['table_header_create_time'] = "Créer l'heure";
$lang ['table_header_guid'] = "Guid";
$lang ['table_header_information'] = "Information";
$lang ['table_header_value'] = "Valeur";

/*Entrée espace réservé Lang*/
$lang ['placeholder_manage_account'] = "Gérer le compte";
$lang ['placeholder_update_information'] = "Mettre à jour les informations du compte";
$lang ['placeholder_donation_logs'] = "Enregistrements de dons";
$lang ['placeholder_store_logs'] = "Enregistrer les enregistrements";
$lang ['placeholder_create_changelog'] = "Créer le journal des modifications";
$lang ['placeholder_edit_changelog'] = "Modifier le journal des modifications";
$lang ['placeholder_create_category'] = "Créer une catégorie";
$lang ['placeholder_edit_category'] = "Modifier la catégorie";
$lang ['placeholder_create_forum'] = "Créer un forum";
$lang ['placeholder_edit_forum'] = "Modifier le forum";
$lang ['placeholder_create_menu'] = "Créer un menu";
$lang ['placeholder_edit_menu'] = "Menu Édition";
$lang ['placeholder_create_news'] = "Créer des nouvelles";
$lang ['placeholder_edit_news'] = "Modifier les actualités";
$lang ['placeholder_create_page'] = "Créer une page";
$lang ['placeholder_edit_page'] = "Modifier la page";
$lang ['placeholder_create_realm'] = "Créer un royaume";
$lang ['placeholder_edit_realm'] = "Modifier le royaume";
$lang ['placeholder_create_slide'] = "Créer une diapositive";
$lang ['placeholder_edit_slide'] = "Modifier la diapo";
$lang ['placeholder_create_item'] = "Créer un élément";
$lang ['placeholder_edit_item'] = "Modifier l'élément";
$lang ['placeholder_create_topsite'] = "Créer un Topsite";
$lang ['placeholder_edit_topsite'] = "Modifier le topsite";
$lang ['placeholder_create_top'] = "Créer un élément TOP";
$lang ['placeholder_edit_top'] = "Modifier l'élément TOP";

$lang ['placeholder_upload_image'] = "Télécharger l'image";
$lang ['placeholder_icon_name'] = "Nom de l'icône";
$lang ['placeholder_category'] = "Catégorie";
$lang ['placeholder_name'] = "Nom";
$lang ['placeholder_item'] = "Article";
$lang ['placeholder_image_name'] = "Nom de l'image";
$lang ['placeholder_reason'] = "Raison";
$lang ['placeholder_gmlevel'] = "niveau GM";
$lang ['placeholder_url'] = "URL";
$lang ['placeholder_child_menu'] = "Menu enfant";
$lang ['placeholder_url_type'] = "type d'URL";
$lang ['placeholder_route'] = "Chemin";
$lang ['placeholder_hours'] = "Heures";
$lang ['placeholder_soap_hostname'] = "Nom d'hôte du savon";
$lang ['placeholder_soap_port'] = "Puerto del Soap";
$lang ['placeholder_soap_user'] = "Utilisateur Soap";
$lang ['placeholder_soap_password'] = "Mot de passe Soap";
$lang ['placeholder_db_character'] = "Caractère";
$lang ['placeholder_db_hostname'] = "Database Hostname";
$lang ['placeholder_db_name'] = "Nom de la base de données";
$lang ['placeholder_db_user'] = "Utilisateur de base de données";
$lang ['placeholder_db_password'] = "Mot de passe de la base de données";
$lang ['placeholder_account_points'] = "Points de compte";
$lang ['placeholder_account_ban'] = "Compte Bannear";
$lang ['placeholder_account_unban'] = "Compte déséquilibré";
$lang ['placeholder_account_grant_rank'] = "Accorder le rang GM";
$lang ['placeholder_account_remove_rank'] = "Supprimer le classement GM";
$lang ['placeholder_command'] = "Commande";

/*Config Lang*/
$lang ['conf_website_name'] = "Nom du site Web";
$lang ['conf_realmlist'] = "Realmlist";
$lang ['conf_discord_invid'] = "ID d'invitation Discord";
$lang ['conf_timezone'] = "Fuseau horaire";
$lang ['conf_theme_name'] = "Nom du sujet";
$lang ['conf_maintenance_mode'] = "Mode de maintenance";
$lang ['conf_social_facebook'] = "URL Facebook";
$lang ['conf_social_twitter'] = "URL Twitter";
$lang ['conf_social_youtube'] = "URL Youtube";
$lang ['conf_paypal_currency'] = "Monnaie PayPal";
$lang ['conf_paypal_mode'] = "Mode PayPal";
$lang ['conf_paypal_client'] = "ID client PayPal";
$lang ['conf_paypal_secretpass'] = "Mot de passe secret PayPal";
$lang ['conf_default_description'] = "Description par défaut";
$lang ['conf_admin_gmlvl'] = "Administrateur GMLevel";
$lang ['conf_mod_gmlvl'] = "Modérateur GMLevel";
$lang ['conf_recaptcha_key'] = "reCaptcha Site Key";
$lang ['conf_account_activation'] = "Activation du compte";
$lang ['conf_smtp_hostname'] = "Nom d'hôte SMTP";
$lang ['conf_smtp_port'] = "Port SMTP";
$lang ['conf_smtp_encryption'] = "Cryptage SMTP";
$lang ['conf_smtp_username'] = "Utilisateur SMTP";
$lang ['conf_smtp_password'] = "Mot de passe SMTP";
$lang ['conf_sender_email'] = "E-mail de l'expéditeur";
$lang ['conf_sender_name'] = "Nom de l'expéditeur";

/* Journaux */
$lang ['placeholder_logs_dp'] = "Don";
$lang ['placeholder_logs_quantity'] = "Quantité";
$lang ['placeholder_logs_hash'] = "Hachage";
$lang ['placeholder_logs_voteid'] = "ID de vote";
$lang ['placeholder_logs_points'] = "Points";
$lang ['placeholder_logs_lasttime'] = "Dernière fois";
$lang ['placeholder_logs_expiredtime'] = "Délai d'expiration";

/* Statut Lang */
$lang ['status_completed'] = "Terminé";
$lang ['status_cancelled'] = "Annulé";

/* Options Lang */
$lang ['option_normal'] = "Normal";
$lang ['option_dropdown'] = "Dropdown";
$lang ['option_image'] = "Image";
$lang ['option_video'] = "Vidéo";
$lang ['option_iframe'] = "Iframe";
$lang ['option_enabled'] = "Activé";
$lang ['option_disabled'] = "Désactivé";
$lang ['option_ssl'] = "SSL";
$lang ['option_tls'] = "TLS";
$lang ['option_everyone'] = "Tout le monde";
$lang ['option_staff'] = "STAFF";
$lang ['option_all'] = "STAFF - All";
$lang ['option_rename'] = "Renommer";
$lang ['option_customize'] = "Personnaliser";
$lang ['option_change_faction'] = "Changer la faction";
$lang ['option_change_race'] = "Changer de race";
$lang ['option_dp'] = "DP";
$lang ['option_vp'] = "VP";
$lang ['option_dp_vp'] = "DP & VP";
$lang ['option_internal_url'] = "URL interne";
$lang ['option_external_url'] = "URL externe";
$lang ['option_on'] = "On";
$lang ['option_off'] = "Off";

/* Comte Lang */
$lang ['count_accounts_created'] = "Comptes créés";
$lang ['count_accounts_banned'] = "Comptes interdits";
$lang ['count_news_created'] = "Nouvelles créées";
$lang ['count_changelogs_created'] = "Changelogs créé";
$lang ['total_accounts_registered'] = "Total des comptes enregistrés.";
$lang ['total_accounts_banned'] = "Total des comptes bannis.";
$lang ['total_news_writed'] = "Total des nouvelles écrites.";
$lang ['total_changelogs_writed'] = "Total des journaux de modifications écrits.";

$lang ['info_alliance_players'] = "Joueurs de l'Alliance";
$lang ['info_alliance_playing'] = "Alliances jouant dans le royaume";
$lang ['info_horde_players'] = "Joueurs de la horde";
$lang ['info_horde_playing'] = "Des hordes jouant dans le royaume";
$lang ['info_players_playing'] = "Joueurs jouant dans le royaume";

/* Lang d'alerte */
$lang ['alert_smtp_activation'] = "Si vous activez cette option, vous devez configurer SMTP pour envoyer des e-mails.";
$lang ['alert_banned_reason'] = "Vous êtes banni, raison:";

/* Logs Lang */
$lang ['log_new_level'] = "Obtenir un nouveau niveau";
$lang ['log_old_level'] = "Avant c'était";
$lang ['log_new_name'] = "Vous avez un nouveau nom";
$lang ['log_old_name'] = "Avant c'était";
$lang ['log_unbanned'] = "Non interdit";
$lang ['log_customization'] = "Obtenir une personnalisation";
$lang ['log_change_race'] = "Obtenir le changement de race";
$lang ['log_change_faction'] = "Obtenir le changement de faction";
$lang ['log_banned'] = "Il a été banni";
$lang ['log_gm_assigned'] = "Rang GM reçu";
$lang ['log_gm_removed'] = "La plage GM a été supprimée";

/* CMS Lang */
$lang ['cms_version_currently'] = "Cette version est en cours d'exécution";
$lang ['cms_warning_update'] = "Lorsque le cms est mis à jour, les paramètres peuvent être restaurés à leur valeur par défaut en fonction des modifications apportées dans chaque version.";
$lang ['cms_php_version'] = "Version PHP";
$lang ['cms_allow_fopen'] = "allow_url_fopen";
$lang ['cms_allow_include'] = "allow_url_include";
$lang ['cms_loaded_modules'] = "Modules chargés";
$lang ['cms_loaded_extensions'] = "Extensions chargées";
